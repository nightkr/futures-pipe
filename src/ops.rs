//! Example ops to be used with `StreamPipe::pipe`. This is not meant to be
//! exhaustive, but just to demonstrate the overall concepts.

use futures::stream::{Map, Stream, StreamExt};

/// Pipeable version of `StreamExt::map`
pub fn map<S, T, F>(f: F) -> impl FnOnce(S) -> Map<S, F>
where
    S: Stream,
    F: FnMut(S::Item) -> T
{
    |stream| stream.map(f)
}
