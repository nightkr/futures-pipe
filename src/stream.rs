use futures::stream::Stream;

/// A `Stream` that can be piped via a function-style op.
///
/// This is implemented on all `Stream`s, and mostly exists as a contrast against `TryStreamPipe`.
///
/// ```
/// use futures::{executor::block_on, stream::{self, StreamExt}};
/// use futures_pipe::{StreamPipe, ops::map};
/// assert_eq!(
///     block_on(
///         stream::iter(vec![0, 1, 2])
///             .pipe(map(|x| x + 1))
///             .collect::<Vec<i32>>()
///     ),
///     [1, 2, 3]
/// );
/// ```
pub trait StreamPipe: Stream + Sized {
    /// Passes a `Stream`  through a function-style op. Equivalent to `pipe_op(stream)`.
    fn pipe<SPiped, F>(self, pipe_op: F) -> SPiped
    where
        SPiped: Stream,
        F: FnOnce(Self) -> SPiped;
}

impl<S: Stream> StreamPipe for S {
    fn pipe<SPiped, F>(self, pipe_op: F) -> SPiped
    where
        SPiped: Stream,
        F: FnOnce(Self) -> SPiped,
    {
        pipe_op(self)
    }
}
